ANHANG: BEISPIELABLÄUFE FÜR GITHUB FLOW

### Beispielablauf Variante ohne Pull-Rebase:

1. Repo auf GitHub anlegen.
2. D1 holt das Repo mit git clone.
3. D2 ebenso.
4. D1 checkt einen neuen Feature Branch aus und arbeitet daran.
5. D2 ebenso.
6. Wenn D1 oder D2 mit dem Feature fertig ist, stellen D1 / D2 einen Pull-Request (Integration des Features in den Main-Branch).
7. Der Pull-Request wird abgewickelt.
8. Nach dem Merge des Features in den Main-Branch wird der Feature-Branch gelöscht.

### Beispielablauf Variante mit Pull-Rebase VOR Pull-Request

1. Repo auf GitHub anlegen.
2. D1 holt das Repo mit git clone.
3. D2 ebenso.
4. D1 checkt einen neuen Feature Branch aus und arbeitet daran.
5. D2 ebenso.
6. Wenn D1 oder D2 mit dem Feature fertig ist, wird folgendes durchgeführt: To update your branch my-feature with recent changes from your default branch (here, using main):
7. Fetch the latest changes from `_main: git fetch origin main_`
8. Check out your feature branch: `_git checkout my-feature_`
9. Rebase it against `main: git rebase origin/main`
10. Force push to your branch (you must have permission to force push branches).
11. If there are merge conflicts, Git prompts you to fix them before continuing the rebase.
12. siehe dazu: https://docs.gitlab.com/ee/topics/git/git_rebase.html:
13. Dann führt D1 einen Pull-Request / Merge-Request aus, mit dem der Feature-Branch in den Basis-Branch integriert wird. Ggf. auftretende Konflikte werden während der Behandlun des PR (online) aufgelöst.
14. Nachdem alle Konflikte aufgelöst sind, wir der Feature Branch in den Base-Branche integriert.
15. Dann löscht D1 den Feature-Branch.
16. D2 macht alles genauso wie D1 ab 6.
17. usw.
18. 