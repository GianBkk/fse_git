# FSE_GIT

## Gian's docu

Ich habe mit dem command 'git branch feature-gian' einen neuen branch erstellt.
Weiter habe ich diesen dann gepublished.
Hier füre ich jetzt eine änderung durch und Peter macht dasselbe auf einen anderen branch.
Da wir beide am gleichen file arbeiten auf verschiedene branches sollte es zu einem issue kommen.
Um das zu lösen sollte der main branch geholt werden mit: 'git pull origin main'.
Und dann auf dem feature branch 'git checkout feature-branch'.
Danach sollte ein 'git rebase origin/main' ausgeführt werden und das Konflikt aufgehoben werden.

# 27.09.2023 Git Dokumentation Peter Chau
### Gitflow 
- einen neuen Branch erstellen: git branch NameDesBranches_#123 dient als hinzufügen neuer Features
- Änderung vornehmen Files, Ordners etc.
  - git commit 
- einen Push Request schicken (kann zu Konflikten kommen) 
  - git push origin develop 
  - dient auch für einen Review
- danach fetchen und Mergen alternative wäre der git Pull (Kombination aus fetch und Merge)
  - wenn es Konflikte gibt, muss der Programmierer es beheben
# ![git.png](git.png)


### bessere Variante wäre mit git rebase anstatt mit pull, sonst wird die git History verschmutzt mit Merge commits (wird in Ablauf erklärt)
## Git Befehle
- git log --oneline

## gute Git Extensions für VS Code
- Git Project Manager
- Git Graph
- Git History
- GitLens
  
  

## Aufgabe 4

### Gitflow:

Charakteristika: Komplexes Branching-Modell mit "master" und "develop" für Stabilität und Entwicklung. Verwendung von Feature-, Release- und Hotfix-Branches.


### GitLab Flow:
Ähnlich wie GitHub Flow, aber mit zusätzlichen Phasen für Code-Überprüfung, Staging und Produktion.

### OneFlow:
Charakteristika: Einfaches Modell mit nur einem Branch (z.B., "main" oder "master"). Betont Continuous Integration und Tests.


| Strategie   | Eigenschaft                        | Vorteile                                         | Nachteile                                           |
| ----------- | ---------------------------------- | ------------------------------------------------ | --------------------------------------------------- |
| Github Flow | Einfach und leicht verständlich    | Schnelle Veröffentlichungen                      | Weniger geeignet für langfristige Projekte          |
| Gitflow     | Komplex mit dauerhaften Branches   | Strukturiertes Branching-Modell                  | Komplexität in kleineren Projekten                  |
| Gitlab Flow | Ähnlich wie GitHub Flow, erweitert | Strukturiert, umfassende Tests und Überprüfungen | Komplexität in kleineren Teams                      |
| One Flow    | Ein einziger dauerhafter Branch    | Einfach, kontinuierliche Integration             | Weniger Kontrolle über getrennte Entwicklungsphasen |

